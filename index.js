// import express dependecy
const express = require('express');
const mongoose = require('mongoose');

// allows our backend application to be available in our forntend application
// cors - cross origin resourse sharing
const cors = require('cors');

// import routes User
const userRoutes = require('./routes/userRoutes');

// import routes Course
const courseRoutes = require('./routes/courseRoutes')

const app = express();
const port = 4000;

// create a connection with mongoDB
mongoose.connect("mongodb+srv://admin_yambao:admin169@batch-169.m3rgy.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});


// create a variable for mongodb connection using mongoose independency
let db = mongoose.connection;


// Notification if connection to db is successful or not
// Catching any error that may occur from the database connection
db.on('error', console.error.bind(console, 'Connection Error'));


// Once successful a message will log a message to inform that connection is has connected to the server
db.once('open', () => console.log('Connected to MongoDB'));

// middleware
app.use(express.json());
app.use(cors());

// use our routes and group together under '/users'
app.use('/users', userRoutes);

// use our routes and group together under '/course'
app.use('/courses', courseRoutes);


// Message if the server is successfully running
app.listen(port, () => console.log(`Server is running at port ${port}`));