const express = require('express');

const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// Routes

// User Registration
router.post("/", userControllers.registerUser);


// Get All Users
router.get("/", userControllers.getAllUsers);


// Login
router.post("/login", userControllers.loginUser);


// Retrieve User Details 
router.get("/getUserDetails", verify, verifyAdmin, userControllers.getUserDetails); //

// Checking of email if already registered
// localhost:4000/users/checkemailExists
router.post('/checkEmailExists', userControllers.checkEmailExists);


// Updating a regular user to an Admin
// localhost:4000/users/updateAdmin/:id
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);


// Updating a user details
// localhost:4000/users/updateUserDetails
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

// Enroll registered user
// localhost:4000/users/enroll
router.post("/enroll", verify, userControllers.enroll);


// Get users enrolled courses
// localhost:4000/users/getEnrollments
router.get("/getEnrollments", verify, userControllers.getEnrollments);



module.exports = router;
