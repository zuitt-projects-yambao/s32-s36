const express = require('express');

const router = express.Router();

// import course courseControllers	 
const courseControllers = require('../controllers/courseControllers');

const auth = require("../auth");

// for verifying if the user is Admin or not
// object destructuring of auth.js
// auth is an imported module, so therefore it is an object is JS
const {verify, verifyAdmin} = auth;


// Posting/Adding a course
// localhost:4000/courses 
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

// Getting a course
//localhost:4000/courses
router.get("/", courseControllers.getAllCourses);

// Getting a Single Course
// localhost:4000/courses/getSingleCourse/:id
router.get("/getSingleCourse/:id", courseControllers.getSingleCourse);


// Activity 4

// Archive a course
// localhost:4000/courses/archive/:id
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);


// Activate a course
// localhost:4000/courses/activate/:id
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);


// Retrieve all active course
//
router.get('/getActiveCourses', courseControllers.getActiveCourses)

// update a course
//
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);


// get inactive course
// 
router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses);


// find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName)


// find courses by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice)


// getEnrolles by id
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);



module.exports = router;

