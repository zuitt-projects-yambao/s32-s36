// import Course model
const Course = require('../models/Course');

// import User model
const User = require('../models/User');

module.exports.addCourse = (req, res) => {
	console.log(req.body);



	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price: req.body.price

	});


	newCourse.save()
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

module.exports.getSingleCourse = (req, res) => {
	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Activity 4

module.exports.archiveCourse = (req, res) => {
	console.log(req.params);

	let archive = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, archive, {new: true})
	.then(status => res.send(status))
	.catch(err => res.send(err));
};



module.exports.activateCourse = (req, res) => {
	console.log(req.params);

	let activate = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, activate, {new:true})
	.then(status => res.send(status))
	.catch(err => res.send(err));
};

module.exports.getActiveCourses = (req, res) => {
	

	// let isActive = {
	// 	isActive: true
	// }

	// console.log(isActive)

	Course.find({isActive: true})
	.then(activeCourses => res.send(activeCourses))
	.catch(err => res.send(err));
};


module.exports.updateCourse = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	};

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


module.exports.getInactiveCourses = (req, res) => {
	Course.find({isActive: false})
	.then(activeCourses => res.send(activeCourses))
	.catch(err => res.send(err));
};


// find courses by name
module.exports.findCoursesByName = (req, res) => {
	console.log(req.body);

	Course.find({name: {$regex: req.body.name, $options: '$i'}} )
	.then(result => {
		if(result.length === 0){
			return res.send("No Courses found");
		}
		else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};


// find courses by price
module.exports.findCoursesByPrice = (req, res) => {
	console.log(req.body);

	Course.find({price: req.body.price})
	.then(result => {
		console.log(result.length);

		if (result.length === 0) {
			return res.send("No course found")
		}
		else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err));
};




module.exports.getEnrollees = (req, res) => {
	console.log(req.params)


	// enrollees : [
	// 	{
	// 		userId : {
	// 			type : String,
	// 			required : [true, "User ID is required"]
	// 		},

	// 		status : {
	// 			type : String,
	// 			default: "Enrolled"
	// 		},

	// 		dateEnrolled :
	// 		{
	// 			type : Date,
	// 			default: new Date()
	// 		}

	// 	}
	// ]
	


	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))
	.catch(err => res.send(err));



}