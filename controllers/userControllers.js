// import bcrypt module
// Note : bcrypt is for adding security
const bcrypt = require("bcrypt");

// import User model
const User = require('../models/User');

// import Course model
const Course = require('../models/Course');

// import auth module
const auth = require("../auth");


// Controllers

// User Registration

module.exports.registerUser = (req, res) => {

	// check inserted data
	console.log(req.body);

	/*
		bcrypt - add a layer of security to your user's password

		What bcrypt does is hash our password into a randomized characted version of the orginal string

		syntax:
			bcrypt.hashSync(<stringToBeHashed> , <saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized
	*/

	// secure password
	const hashedPW = bcrypt.hashSync(req.body.password, 10);


	// Create a new user document our of our user model

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

module.exports.loginUser = (req, res) => {
	console.log(req.body);

	/*
		1. find by user email
		2. if a user email is found, check the password
		3. if we don't find the user email, send a message
		4. if upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client 
	*/


	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("User does not exist");
		}
		else {
			// Comparing of password
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})

			}
			else{
				return res.send("Password is incorrect");
			}
		}
		
	})
	.catch(err => res.send(err));
};

module.exports.getUserDetails = (req, res) => {

	// getting from auth.js, for verifying if user is Admin
	/*
		A payload in API is the actual data pack that is sent with the GET method in HTTP. It is the crucial information that you submit to the server when you are making an API request. The payload can be sent or received in various formats, including JSON.
	*/
	console.log(`This is a payload:`);
	console.log(req.user);

	// for testing
	// console.log(`this is the id: ${req.user.id}`)


	// 1. find a logged in user's document from our db and sent it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

};


// Checking email if used or available
module.exports.checkEmailExists = (req, res) => {
	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail === null){
			return res.send("Email is available!");
		}
		else{
			return res.send("Email is already registered!");
		}
	})
	// .then(result => res.send(result))
	.catch(err => res.send(err));
};


// Updating a regular user to admin
module.exports.updateAdmin = (req, res) => {
	// req.user.id captures the token's id
	console.log(`This will capture the admins ID : ${req.user.id}`);
	// req.params.id captures the id designated in the params
	console.log(req.params.id);

	let update = {
		isAdmin : true
	};

	User.findByIdAndUpdate(req.params.id, update, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));


};


// Update User Details
module.exports.updateUserDetails = (req, res) => {
	// check the input for new values for our user's details
	console.log(req.body);
	
	// check the logged in user's id
	console.log(req.user.id);

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


// Enroll Student

// async



module.exports.enroll = async (req, res) => {
	/*
		Enrollment Process:

		1. Look for the user by its id.
			>>push the details of the course we're trying to enroll in.
				>> we'll push to a new enrollment subdocument in our user

		2. Look for the course by its id.
			>> push the details of the enrollee/user who's trying to enroll.
				>> we'll push to a new enrolles subdocument in our course

		3. When both saving documents are successful, we send a message to the client
	*/

	console.log(req.user.id);
	console.log(req.body.courseId);

	// Checking if a user is an admin, if he is an admin then he should not be able to enroll
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	/*
		Find the user:

		async - a keyword that allows us to make our function aynchronous. Which means, that instead of JS regular behavior of running each code line by line, it will allow us to wait for the result of the function

		await - a keyword that that allows us to wait for the function to finish before proceeding
	*/

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

			console.log(user);

			// add the courseId in an object and push that object into user's enrollment array
			let newEnrollment = {
				courseId: req.body.courseId
			};

			console.log(newEnrollment);

			// The push() method adds new items to the end of an array
			user.enrollments.push(newEnrollment);
			
			return user.save().then(user => true).catch(err => err.message)
	})



	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with message
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}



	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		console.log(course);

		// create an object 
		let enrollee = {
			userId: req.user.id
		};

		course.enrollees.push(enrollee);



		return course.save().then(course => true).catch(err => err.message)
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}


	if (isUserUpdated && isCourseUpdated) {
		return res.send({message: 'Enrolled Succesfully!'})
	}
};


// module.exports.getEnrollments = (res, req) => {
// 	console.log(req.user);

// 	console.log(`This is a payload:`);
// 	console.log(req.user);

// 	User.findById({id: req.body.id})
// 	.then(result => res.send(result))
// 	.catch(err => res.send(result));
// }


module.exports.getEnrollments = (req, res) => {

	// getting from auth.js, for verifying if user is Admin
	/*
		A payload in API is the actual data pack that is sent with the GET method in HTTP. It is the crucial information that you submit to the server when you are making an API request. The payload can be sent or received in various formats, including JSON.
	*/
	console.log(`This is a payload:`);
	console.log(req.user);

	//const myJSON = JSON.stringify(obj);



	// let enrollment = ({enrollments : req.user.enrollments});

	// console.log(enrollments);

	// for testing
	// console.log(`this is the id: ${req.user.id}`)


	// 1. find a logged in user's document from our db and sent it to the client by its id

	// let newEnrollment = { enrollments: {
	// 	courseId: req.user.courseId,
	// 	status: req.user.status,
	// 	date: req.user.date,
	// 	id: req.user.id
	// 	}
	// };
	// console.log(newEnrollment);


	// let enrollee = {
	// 		userId: req.user.id
	// 	};
	let enrollment = {
		courseID: req.user.courseId
	}

	console.log(req.user.id);

	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err));


	// let isCourseEnrolled= await Course.findById(req.body.courseId).then(course => {

	// 	console.log(course);

	// 	// create an object 
	// 	let enrollee = {
	// 		userId: req.user.id
	// 	};

	// 	course.enrollees.push(enrollee);



	// 	return course.save().then(course => true).catch(err => err.message)
	// })
};